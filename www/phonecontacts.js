/*global cordova, module*/

module.exports = {
    getContacts: function (input, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "PhoneContacts", "getContacts", [input]);
    }
};
